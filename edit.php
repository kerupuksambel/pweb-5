<?php
    include('include/connect.php');
    $id = $_GET['id'];

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $nama = $conn->real_escape_string($_POST['nama']);
        $email = $conn->real_escape_string($_POST['email']);
        $alamat = $conn->real_escape_string($_POST['alamat']);
        $no_telp = $conn->real_escape_string($_POST['no_telp']);

        $q = sprintf("UPDATE guests SET nama = '%s', email = '%s', alamat = '%s', no_telp = '%s' WHERE id = %d", $nama, $email, $alamat, $no_telp, $id);
    
        $q = $conn->query($q);
        header('Location: index.php');
    
    }else{
        include('include/inc.php');

        $q = $conn->real_escape_string(sprintf("SELECT * FROM guests WHERE id = %d", $id));
        $q = $conn->query($q);
        
        $res = $q->fetch_assoc();
        $nama = $res['nama'];
        $email = $res['email'];
        $alamat = $res['alamat'];
        $no_telp = $res['no_telp'];
?>
    <div class="container-fluid">
        <form method="POST">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" value="<?= $nama ?>"/>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control" value="<?= $email ?>"/>
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="alamat" class="form-control" value="<?= $alamat ?>"/>
            </div>
            <div class="form-group">
                <label>No Telp</label>
                <input type="text" name="no_telp" class="form-control" value="<?= $no_telp ?>"/>
            </div>
            <input type="submit" class="btn btn-primary" value="Edit">
        </form>
    </div>
<?php
    }
?>