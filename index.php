<?php session_start(); ?>
<html>
    <head>
        <?php
            include('include/inc.php');
        ?>
    </head>
    <body>
        <div class="container-fluid">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th>No. Telp</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        include('include/connect.php');
    
                        $q = $conn->real_escape_string('SELECT * FROM guests');
                        $q = $conn->query($q);
                        $i = 1;
                        while($res = $q->fetch_assoc()):
                    ?>
    
                    <tr>
                        <td class="align-middle"><?= $i++ ?></td>
                        <td class="align-middle"><?= $res['nama'] ?></td>
                        <td class="align-middle"><?= $res['alamat'] ?></td>
                        <td class="align-middle"><?= $res['email'] ?></td>
                        <td class="align-middle"><?= $res['no_telp'] ?></td>
                        <td>
                            <a href="edit.php?id=<?= $res['id'] ?>" role="button" class="btn btn-sm btn-primary">Edit</a>
                            <a href="javascript:rm('delete.php?id=<?= $res['id'] ?>')" role="button" class="btn btn-sm btn-danger">Hapus</a>
                        </td>
                    </tr>
                    
                    <?php endwhile; ?>
                </tbody>
            </table>
            <a href="create.php" role="button" class="btn btn-primary">Tambah</a>
        </div>
    </body>
    <script type="text/javascript">
        function rm(s){
            var c = confirm('Apakah Anda yakin akan menghapus?')
            if(c){
                window.location = s;
            }
        }
    </script>
</html>
