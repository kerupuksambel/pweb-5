<?php
    include('include/connect.php');

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $nama = $conn->real_escape_string($_POST['nama']);
        $email = $conn->real_escape_string($_POST['email']);
        $alamat = $conn->real_escape_string($_POST['alamat']);
        $no_telp = $conn->real_escape_string($_POST['no_telp']);

        $q = $conn->query(sprintf("INSERT INTO guests (nama, email, alamat, no_telp) VALUES ('%s', '%s', '%s', '%s')", $nama, $email, $alamat, $no_telp));    
        header('Location: index.php');
    
    }else{
        include('include/inc.php');
?>
    <div class="container-fluid">
        <form method="POST">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="alamat" class="form-control"/>
            </div>
            <div class="form-group">
                <label>No Telp</label>
                <input type="text" name="no_telp" class="form-control"/>
            </div>
            <input type="submit" class="btn btn-primary" value="Tambah">
        </form>
    </div>
<?php
    }
?>