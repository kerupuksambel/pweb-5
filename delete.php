<?php
    include('include/connect.php');

    $q = $conn->query($conn->real_escape_string(sprintf("DELETE FROM guests WHERE id = %d", $_GET['id'])));
    if(!$conn->error){
        header('Location: index.php');
    }
?>